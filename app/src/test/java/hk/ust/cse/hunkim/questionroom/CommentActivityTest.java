package hk.ust.cse.hunkim.questionroom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.WorkerThread;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.logging.Handler;

import hk.ust.cse.hunkim.questionroom.question.Question;

import static android.support.v4.app.ActivityCompat.startActivity;
import static android.support.v4.content.ContextCompat.startActivities;


/**
 * Created by Tichy on 30/10/15.
 */

/**
 * TODO Should for some reason the test not run, please ignore this class
 */
public class CommentActivityTest extends ActivityInstrumentationTestCase2<CommentActivity> {
    private Intent intent;
    private TextView tx;
    private Button btnLike;
    private Button btnDislike;
    private Bundle bundle;
    private Activity activity;
    CommentActivity ca;

    public CommentActivityTest(){
        super(CommentActivity.class);
    }

    /*
    Intent myIntent = new Intent(this, NewActivityClassName.class);
    myIntent.putExtra("firstKeyName","FirstKeyValue");
    myIntent.putExtra("secondKeyName","SecondKeyValue");
    startActivity(myIntent);
    */

    @Override
    protected void setUp() throws Exception{
        super.setUp();

        Bundle bundle = new Bundle();
        bundle.putString("topic", "compTesting");
        bundle.putString("key", "-K3neKC0ILH2OUc6nxU2");
        bundle.putString("roomName", "roomNameTest");
        bundle.putStringArray("tags", null);
        bundle.putString("op", "5");
        bundle.putString("time", "1728");

        intent = new Intent();
        intent.putExtras(bundle);

        setActivityIntent(intent);
        activity = getActivity();

        //tx = (TextView)activity.findViewById(R.id.textView);
        //btnDislike = (Button)activity.findViewById(R.id.d_echo);


    }


    /*@MediumTest
    public void testPreconditions(){//For testing if everything is set up correctly before start testing
        //startActivity(ca, null, null);

        tx = (TextView)getActivity().findViewById(R.id.textView);
        btnLike = (Button)getActivity().findViewById(R.id.echo);
        btnDislike = (Button)getActivity().findViewById(R.id.d_echo);

        assertNotNull("CommentActivity instance is null", );
        assertNotNull("TextView instance is null", tx);
        assertNotNull("Like Button instance is null", btnLike);
        assertNotNull("Dislike Button instance is null", btnDislike);
    }*/

    @SmallTest
    public void testCommentActivity_btnLike(){
        btnLike = (Button)activity.findViewById(R.id.echo);
        assertEquals("Button Like doesn't start at 0", "", btnLike.getText());
    }

    @SmallTest
    public void testAddComment(){

        getActivity().addComment("lalalala");//lalalala Kin Fu laaaa la
    }
}
