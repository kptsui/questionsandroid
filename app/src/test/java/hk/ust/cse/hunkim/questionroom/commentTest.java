package hk.ust.cse.hunkim.questionroom;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

import hk.ust.cse.hunkim.questionroom.question.comment;

/**
 * Created by Tichy on 22/11/15.
 */
public class commentTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();

    }

    @SmallTest
    public void testComment(){
        comment c = new comment();

        assertEquals("comment", "dateString", c.dateString);
        assertEquals("comment", "default_name", c.name);
        assertEquals("comment", "default_msg", c.msg);
    }
}