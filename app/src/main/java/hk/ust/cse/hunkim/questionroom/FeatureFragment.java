package hk.ust.cse.hunkim.questionroom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class FeatureFragment extends Fragment {

    private ImageView guideImage;
    private Button btnNextPage;
    private int currentPage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feature, container, false);
        guideImage = (ImageView) view.findViewById(R.id.guideImage);
        btnNextPage = (Button) view.findViewById(R.id.btnNextPage);
        currentPage = 0;
        btnNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentPage++;
                if(currentPage == 1)
                    guideImage.setImageResource(R.drawable.guide1);
                else if(currentPage == 2){
                    guideImage.setImageResource(R.drawable.guide2);
                    btnNextPage.setText("Finish");
                }
                else {
                    getActivity().getSupportFragmentManager().popBackStack();
                    return;
                }
            }
        });
        return view;
    }
}
