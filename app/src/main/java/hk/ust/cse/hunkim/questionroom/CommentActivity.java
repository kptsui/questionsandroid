package hk.ust.cse.hunkim.questionroom;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.GenericTypeIndicator;
import com.firebase.client.ValueEventListener;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hk.ust.cse.hunkim.questionroom.db.DBHelper;
import hk.ust.cse.hunkim.questionroom.db.DBUtil;
import hk.ust.cse.hunkim.questionroom.question.Question;
import hk.ust.cse.hunkim.questionroom.question.comment;

import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconTextView;
import com.rockerhieu.emojicon.emoji.Emojicon;

public class CommentActivity extends FragmentActivity implements PopupMenu.OnMenuItemClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    // TODO: change this to your own Firebase URL
    private static final String FIREBASE_URL = "https://intense-torch-3848.firebaseio.com/";

    private Firebase mFirebaseRef;

    private String topicString;
    private String questionKey;
    private String roomName;
    private String[] tags;
    private String op;
    private String time;

    private String content;

    private ArrayList<comment> comments;

    private DBUtil dbutil;

    public DBUtil getDbutil() {
        return dbutil;
    }

    private Button backButton;
    private Button likeButton;
    private Button dislikeButton;
    private Button commentButton;

    private LinearLayout commentList;

    private boolean isEmojiClick = false;
    private EmojiconsFragment emojiFragment;

    private EmojiconEditText mEditEmojicon;
    private ImageButton imageButton;

    private Context context;
    private InputMethodManager inputMethodManager;
    private EditText inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialized once with an Android context.
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_comment);

        Bundle bundle = this.getIntent().getExtras();
        topicString = bundle.getString("topic");
        questionKey = bundle.getString("key");
        roomName = bundle.getString("roomName");
        tags = bundle.getStringArray("tags");
        op = bundle.getString("op");
        time = bundle.getString("time");

        inputText = (EditText) findViewById(R.id.messageInput);
        EmojiconTextView topic = (EmojiconTextView) findViewById(R.id.topic);

        content = topicString.replace(Question.extractYoutubeLink(topicString), "");
        content = content.replace(Question.extractImageLink(content), "");
        topic.setText(content);

        TextView header = (TextView) findViewById(R.id.roomName);
        header.setText(roomName);
        TextView opTextView = (TextView) findViewById(R.id.op);
        opTextView.setText(op);
        TextView timeTextView = (TextView) findViewById(R.id.time);
        timeTextView.setText(time);

        comments = new ArrayList<>();

        mFirebaseRef = new Firebase(FIREBASE_URL).child(roomName).child("questions").child(questionKey);

        // get the DB Helper
        DBHelper mDbHelper = new DBHelper(this);
        dbutil = new DBUtil(mDbHelper);

        backButton = (Button) findViewById(R.id.backToMain);
        likeButton = (Button) findViewById(R.id.echo);
        dislikeButton = (Button) findViewById(R.id.d_echo);
        commentButton = (Button) findViewById(R.id.commentButton);

        backButton.setOnClickListener(buttonListener);
        likeButton.setOnClickListener(buttonListener);
        dislikeButton.setOnClickListener(buttonListener);
        commentButton.setOnClickListener(buttonListener);

        commentList = (LinearLayout) findViewById(R.id.layout_commentList);

        mEditEmojicon = (EmojiconEditText) findViewById(R.id.messageInput);
        imageButton = (ImageButton) findViewById(R.id.imageButton);
        imageButton.setOnClickListener(buttonListener);

        emojiFragment = EmojiconsFragment.newInstance(false);
        SetEmojiconFragment();

        context = this.getApplicationContext();
        inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);

        if (tags != null) {
            LinearLayout hashTagLayout = (LinearLayout) findViewById(R.id.hashtag);
            for (int i = 0; i < tags.length; ++i) {
                addTagsToView(hashTagLayout, tags[i]);
            }
        }
        WebView youtubeFrame = createYoutubeView(Question.extractYoutubeLink(topicString));
        if(youtubeFrame != null){
            FrameLayout topicYoutubeFrame = (FrameLayout) findViewById(R.id.topicYoutubeFrame);
            topicYoutubeFrame.addView(youtubeFrame);
        }
        WebView imageWebView = createImageWebView(Question.extractImageLink(topicString));
        if(imageWebView != null){
            FrameLayout topicImageFrame = (FrameLayout) findViewById(R.id.topicImageFrame);
            topicImageFrame.addView(imageWebView);
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        mFirebaseRef.child("comments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // put comment data into ArrayList<comment> comments
                GenericTypeIndicator<List<comment>> t = new GenericTypeIndicator<List<comment>>() {
                };
                List<comment> messages = snapshot.getValue(t);

                if (messages != null) {
                    int currentElements = commentList.getChildCount();
                    int max = messages.size();
                    for (int i = currentElements; i < max; ++i) {
                        comment message = messages.get(i);
                        comment temp = new comment();
                        temp.name = message.name;
                        temp.msg = message.msg;
                        temp.dateString = message.dateString;
                        comments.add(temp);
                        addCommentView(temp.name, temp.msg, temp.dateString);
                    }
                    mFirebaseRef.child("views").setValue(comments.size());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(getApplicationContext(), "The read failed: " + firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        mFirebaseRef.child("echo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                likeButton.setText("" + snapshot.getValue());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        mFirebaseRef.child("d_echo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                dislikeButton.setText("" + snapshot.getValue());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    private Button.OnClickListener buttonListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.backToMain:
                    finish();
                    break;
                case R.id.setting:
                    showPopupMenu(view);
                    break;
                case R.id.echo:
                    updateEcho(questionKey);
                    break;
                case R.id.d_echo:
                    updateD_echo(questionKey);
                    break;
                case R.id.commentButton:
                    String input = inputText.getText().toString();
                    addComment(input);
                    break;
                case R.id.imageButton:
                    popUpEmoji();
                    break;
                default:
                    break;
            }
        }
    };

    public void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.setting_menu);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changeName:
                showSetNameDialog();
                return true;
            case R.id.about:
                Toast.makeText(this, "Hello, we are love!sung", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    public void showSetNameDialog() {
        SetNameDialog dialog = new SetNameDialog();
        dialog.show(getFragmentManager(), "setNameDialog");
    }

    // Save to Firebase
    // Adding a comment to a Question Post
    public void addComment(final String input) {
        if (!input.equals("")) {
            try {
                final Firebase commentRef = mFirebaseRef.child("comments");
                // It is triggered one time and then will not be triggered again
                commentRef.addListenerForSingleValueEvent(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                /*
                                Map groupNames = snapshot.getValue();
                                List groupNamesList = new ArrayList(groupNames.values());
                                */
                                GenericTypeIndicator<List<comment>> t = new GenericTypeIndicator<List<comment>>() {
                                };
                                List<comment> messages = dataSnapshot.getValue(t);

                                ArrayList<comment> tempComments = new ArrayList<comment>();

                                if (messages != null) {
                                    for (comment getComment : messages) {
                                        tempComments.add(getComment);
                                    }
                                }

                                comment newComment = new comment();
                                SimpleDateFormat df = new SimpleDateFormat("hh:mm   dd/MM/yyy");
                                Date date = new Date();
                                newComment.dateString = df.format(date);
                                newComment.name = JoinActivity.userName;
                                newComment.msg = input;
                                tempComments.add(newComment);

                                // save data to firebase, actually rewrite it
                                commentRef.setValue(tempComments);
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        }
                );
                // reset input to empty
                inputText.setText("");
                // hiding input when message sent
                if (isEmojiClick)
                    hideEmojiconFragment();
                inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
                Log.e("Error", "addComment()");
            }
        }
    }

    public void updateEcho(String key) {
        if (dbutil.contains(key)) {
            Log.e("Dupkey", "Key is already in the DB!");
            return;
        }

        final Firebase echoRef = mFirebaseRef.child("echo");
        echoRef.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long echoValue = (Long) dataSnapshot.getValue();
                        Log.e("echo update:", "" + echoValue);

                        echoRef.setValue(echoValue + 1);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                }
        );
        // Update SQLite DB
        dbutil.put(key);
    }

    public void updateD_echo(String key) {
        if (dbutil.contains(key)) {
            Log.e("Dupkey", "Key is already in the DB!");
            return;
        }

        final Firebase d_echoRef = mFirebaseRef.child("d_echo");
        d_echoRef.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long d_echoValue = (Long) dataSnapshot.getValue();
                        Log.e("d_echo update:", "" + d_echoValue);

                        d_echoRef.setValue(d_echoValue + 1);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                }
        );
        // Update SQLite DB
        dbutil.put(key);
    }

    private void addCommentView(String name, String msg, String date) {
        String youtubeLink = Question.extractYoutubeLink(msg);
        String imageLink = Question.extractImageLink(msg);
        String plainText = msg.replace(youtubeLink, "").replace(imageLink, "");

        // Initialize LinearLayout to be added dynamically for new comments
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setBackgroundColor(Color.WHITE);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.setMargins(0, 5, 0, 0);
        linearLayout.setLayoutParams(param);

        // Dynamically set user name textview
        TextView userNameTextView = new TextView(this);
        LinearLayout.LayoutParams paramNameTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        paramNameTextView.setMargins(20, 10, 0, 0);
        userNameTextView.setLayoutParams(paramNameTextView);
        userNameTextView.setTextSize(16);
        userNameTextView.setTypeface(null, Typeface.BOLD);
        userNameTextView.setTextColor(Color.parseColor("#828282"));

        // Initialize TextView for a comment to be added into LinearLayout
        EmojiconTextView commentTextView = new EmojiconTextView(context);
        LinearLayout.LayoutParams paramTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        paramTextView.setMargins(20, 20, 0, 0);
        commentTextView.setLayoutParams(paramTextView);
        commentTextView.setTextSize(16);
        commentTextView.setTextColor(Color.parseColor("#828282"));

        TextView dateText = new TextView(this);
        LinearLayout.LayoutParams paramDateText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        paramDateText.setMargins(0, 0, 20, 0);
        paramDateText.gravity = Gravity.RIGHT;
        dateText.setLayoutParams(paramDateText);
        dateText.setTextSize(10);
        dateText.setTextColor(Color.parseColor("#828282"));

        userNameTextView.setText(name);
        commentTextView.setText(plainText);
        linearLayout.addView(userNameTextView);
        linearLayout.addView(commentTextView);
        /*
        if(commentString.contains("jpg")){
            WebView web = new WebView(this);
            web.loadUrl("http://vignette4.wikia.nocookie.net/evchk/images/8/82/Hkg058.jpg");
            linearLayout.addView(web);
        }*/

        WebView youtubeFrame = createYoutubeView(youtubeLink);
        if(youtubeFrame != null)
            linearLayout.addView(youtubeFrame);
        WebView imageWebView = createImageWebView(imageLink);
        if(imageWebView != null)
            linearLayout.addView(imageWebView);


        dateText.setText(date);
        linearLayout.addView(dateText);

        commentList.addView(linearLayout);
    }

    public WebView createYoutubeView(String link) {
        if ( link != "" ) {

            WebView youtubeWebView = new WebView(this);
            youtubeWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }
            });
            WebSettings webSettings = youtubeWebView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            // https://youtu.be/6rhRdNfUCxU or ?v=
            String code = null;
            if(link.contains(".be/"))
                code = link.substring(link.indexOf(".be/")+4);
            else if(link.contains("?v="))
                code = link.substring(link.indexOf("?v=")+3);

            String frame = "<iframe width=\"300\" height=\"220\" src=\"https://www.youtube.com/embed/" + code + "\" frameborder=\"0\" allowfullscreen></iframe>";
            youtubeWebView.loadData(frame, "text/html", "utf-8");
            return youtubeWebView;
        }
        return null;
    }

    public WebView createImageWebView(String link){
        if ( link != "" ) {
            WebView image = new WebView(this);
            String imgtag = "<img src=" + link + " width=\"260\" height=\"260\">";

            image.loadData(imgtag, "text/html", "utf-8");
            return image;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        if (isEmojiClick) {
            isEmojiClick = false;
            hideEmojiconFragment();
            inputMethodManager.showSoftInput(mEditEmojicon, 0);
        } else {
            super.onBackPressed();
        }
    }

    /*
    Below are all Emoji Functions
    */
    public void popUpEmoji() {
        isEmojiClick = !isEmojiClick;
        if (isEmojiClick) {
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
            showEmojiconFragment();
        } else {
            hideEmojiconFragment();
        }
    }

    private void showEmojiconFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .show(emojiFragment)
                .commit();
    }

    private void hideEmojiconFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .hide(emojiFragment)
                .commit();
    }

    private void SetEmojiconFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, emojiFragment)
                .hide(emojiFragment)
                .commit();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(mEditEmojicon, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(mEditEmojicon);
    }

    private void addTagsToView(LinearLayout hashTagLayout, String tagStr) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 50);// dp X 2 = px
        params.setMargins(20, 0, 0, 0); // dp X 2 = px

        TextView buttonTag = new TextView(this);
        buttonTag.setLayoutParams(params);
        buttonTag.setTextSize(12);
        buttonTag.setTextColor(Color.WHITE);
        buttonTag.setBackgroundColor(MainActivity.tagColor);
        buttonTag.setPadding(10, 0, 10, 0);
        buttonTag.setText(tagStr);

        hashTagLayout.addView(buttonTag);
    }

    // Image Upload
    public void toImageUploadActivity(View v) {
        Intent intent = new Intent(this, ImageUploadActivity.class);
        intent.putExtra("header", "New Reply");
        startActivityForResult(intent, ImageUploadActivity.DATA_BACK_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ImageUploadActivity.DATA_BACK_REQUEST && resultCode == RESULT_OK) {
            String msg = data.getStringExtra("msg");
            addComment(msg);
        }
    }
}