package hk.ust.cse.hunkim.questionroom;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.GenericTypeIndicator;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import hk.ust.cse.hunkim.questionroom.db.DBUtil;
import hk.ust.cse.hunkim.questionroom.question.Question;

/**
 * @author greg
 * @since 6/21/13
 * <p/>
 * This class is an example of how to use FirebaseListAdapter. It uses the <code>Chat</code> class to encapsulate the
 * data for each individual chat message
 */
public class QuestionListAdapter extends FirebaseListAdapter<Question> {
    private final static long ONE_MINUTE = 60000;
    private final static long ONE_HOUR = 3600000;
    private final static long ONE_DAY = 86400000;
    private static final String FIREBASE_URL = "https://intense-torch-3848.firebaseio.com/";
    private Firebase mFirebaseRef;

    // The mUsername for this client. We use this to indicate which messages originated from this user
    private String roomName;
    MainActivity activity;

    public QuestionListAdapter(Query ref, Activity activity, int layout, String roomName) {
        super(ref, Question.class, layout, activity);
        this.roomName = roomName;
        // Must be MainActivity
        assert (activity instanceof MainActivity);
        this.activity = (MainActivity) activity;
    }

    /**
     * Bind an instance of the <code>Chat</code> class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single <code>Chat</code> instance that represents the current data to bind.
     *
     * @param view     A view instance corresponding to the layout we passed to the constructor.
     * @param question An instance representing the current state of a chat message
     */
    @Override
    protected void populateView(View view, final Question question) {
        DBUtil dbUtil = activity.getDbutil();

        // Map a Chat object to an entry in our listview

        // TextView postDateTime
        Date nowDate = new Date();
        long now = nowDate.getTime();
        long time = question.getTimestamp();
        long diff = now - time;

        final TextView postDateTime = (TextView) view.findViewById(R.id.postDateTime);
        //less than one minute, 1000*60
        if (diff < ONE_MINUTE)
            postDateTime.setText("a minute ago by ");
        //less than 1 hour, 1000*60*60
        else if (diff < ONE_HOUR){
            int intTime = (int) (diff / ONE_MINUTE);
            postDateTime.setText("" + intTime + " minutes ago by ");
        }
        //less than one day, 1000*60*60*24
        else if (diff < ONE_DAY){
            int intTime = (int) (diff / ONE_HOUR);
            postDateTime.setText("" + intTime + " hours ago by ");
        }
        else{
            int intTime = (int) (diff / ONE_DAY);
            postDateTime.setText("" + intTime + " days ago by ");
        }

        // Set opName
        final TextView opName = (TextView) view.findViewById(R.id.opName);
        opName.setText(question.getOp());

        // Set HashTag
        String[] tags = question.getTags();
        if (tags != null) {
            final LinearLayout hashTagLayout = (LinearLayout) view.findViewById(R.id.hashtag);
            hashTagLayout.removeAllViews();
            for(int i=0; i < tags.length; ++i){
                addTagsToView(hashTagLayout, tags[i]);
            }
        }


        // echo button
        int echo = question.getEcho();
        Button echoButton = (Button) view.findViewById(R.id.echo);
        echoButton.setText("" + echo);

        echoButton.setTag(question.getKey()); // Set tag for button

        echoButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MainActivity m = (MainActivity) view.getContext();
                        m.updateEcho((String) view.getTag());
                    }
                }

        );

        // d_echo button
        int d_echo = question.getD_echo();
        Button d_echoButton = (Button) view.findViewById(R.id.d_echo);
        d_echoButton.setText("" + d_echo);

        d_echoButton.setTag(question.getKey()); // Set tag for button

        d_echoButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MainActivity m = (MainActivity) view.getContext();
                        m.updateD_echo((String) view.getTag());
                    }
                }

        );

        // views button
        final String tempp = question.getKey();
        Button viewsButton = (Button) view.findViewById(R.id.views);
        viewsButton.setText("" + question.getViews());

        viewsButton.setTag(question.getKey()); // Set tag for button
        viewsButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MainActivity m = (MainActivity) view.getContext();
                        String time = postDateTime.getText().toString().replace(" by ", "");
                        String op = opName.getText().toString();
                        m.toCommentActivity(tempp, question.getWholeMsg(), question.getTags(), op, time);
                    }
                }

        );

        String msgString = "";

        question.updateNewQuestion();
        if (question.isNewQuestion()) {
            msgString += "<font color=red>NEW </font>";
        }
        //String header = question.getHead().replaceAll("\\<.*?>", "");
        //msgString += "<B>" + header + "</B>" + question.getDesc();
        msgString += question.getHead();

        TextView topicTextView = (TextView) view.findViewById(R.id.head_desc);
        topicTextView.setText(Html.fromHtml(msgString));
        view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        MainActivity m = (MainActivity) view.getContext();
                                        String time = postDateTime.getText().toString().replace(" by ", "");
                                        String op = opName.getText().toString();
                                        m.toCommentActivity(tempp, question.getWholeMsg(), question.getTags(), op, time);
                                    }
                                }

        );

        // check if we already clicked
        boolean clickable = !dbUtil.contains(question.getKey());
/*
        echoButton.setClickable(clickable);
        echoButton.setEnabled(clickable);
*/
        // http://stackoverflow.com/questions/8743120/how-to-grey-out-a-button
        // grey out our button
        /*
        if (clickable) {
            echoButton.getBackground().setColorFilter(null);
        } else {
            echoButton.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
        }*/


        view.setTag(question.getKey());  // store key in the view
    }

    @Override
    protected void sortModels(List<Question> mModels) {
        Collections.sort(mModels, Question.sortingComparator);
    }

    @Override
    protected void setKey(String key, Question model) {
        model.setKey(key);
    }

    private void addTagsToView(LinearLayout hashTagLayout, String tagStr){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 50);// dp X 2 = px
        params.setMargins(20, 5, 0, 0); // dp X 2 = px

        TextView buttonTag = new TextView(activity);
        buttonTag.setLayoutParams(params);
        buttonTag.setTextSize(12);
        buttonTag.setTextColor(Color.WHITE);
        buttonTag.setBackgroundColor(activity.tagColor);
        buttonTag.setPadding(10, 0, 10, 0);
        buttonTag.setText(tagStr);

        hashTagLayout.addView(buttonTag);
    }
}
