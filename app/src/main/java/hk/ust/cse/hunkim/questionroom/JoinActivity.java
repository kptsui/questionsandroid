package hk.ust.cse.hunkim.questionroom;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Handler;
import java.util.logging.LogRecord;


/**
 * A login screen that offers login via email/password.
 */
public class JoinActivity extends FragmentActivity implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {
    public static final String ROOM_NAME = "Room_name";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    // UI references.
    private TextView roomNameView;
    private GridLayout gridLayout;
    private ArrayList<String> roomName;

    public static SharedPreferences preference;
    public static String userName;

    private LinearLayout layoutContainer;
    private ProgressBar progressBar;
    private static int progressStatus = 0;

    private final static String[] courseName = {"comp", "chem", "acct", "hlth", "huma", "lifs", "sbmt", "ceng", "civl", "elec", "ielm", "mech", "scie"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_join);

        preference = getSharedPreferences("fileUserName", MODE_PRIVATE);
        userName = preference.getString("userName", "unknown"); // return unknown when key not found
        if(userName.equals("unknown")){
            Random rand = new Random();
            int n = rand.nextInt(999) + 1;
            userName = "Person" + n;
        }

        roomName = new ArrayList<String>();
        gridLayout = (GridLayout) findViewById(R.id.gridLayout);


        // Set up the login form.
        roomNameView = (TextView) findViewById(R.id.room_name);

        roomNameView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    attemptJoin(textView);
                }
                return true;
            }
        });

        layoutContainer = (LinearLayout) findViewById(R.id.layoutContainer);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    @Override
    protected void onStart() {
        super.onStart();

        gridLayout.setVisibility(View.INVISIBLE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(progressStatus < 100){
                    progressStatus++;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          progressBar.setProgress(progressStatus);
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        gridLayout.setVisibility(View.VISIBLE);
                    }
                });
            }
        }).start();

        Firebase ref = new Firebase("https://intense-torch-3848.firebaseio.com/");
        //ref.addListenerForSingleValueEvent(new ValueEventListener() {
        ref.addValueEventListener(new ValueEventListener() {
            // download the entire group list :(
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot child : snapshot.getChildren()) {
                    String nameOfRoom = child.getKey();
                    if (!roomName.contains(nameOfRoom))
                        roomName.add(nameOfRoom);
                }
                gridLayout.removeAllViews();

                int i = 0;
                for (String temp : roomName) {
                    addGridLayoutContent(temp, gridLayout, i % 3);
                    ++i;
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // ignore
            }
        });

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptJoin(View view) {
        // Reset errors.
        roomNameView.setError(null);

        // Store values at the time of the login attempt.
        String room_name = roomNameView.getText().toString().toLowerCase();

        boolean cancel = false;

        // Check for a valid room_name
        if (TextUtils.isEmpty(room_name)) {
            roomNameView.setError(getString(R.string.error_field_required));

            cancel = true;
        }
        else if (!isEmailValid(room_name)) {
            roomNameView.setError(getString(R.string.error_invalid_room_name));
            cancel = true;
        }
        else if(room_name.length() != 8){
            roomNameView.setError("Please Follow the Format: comp3111");
            cancel = true;
        }
        else{
            String prefix = room_name.substring(0,4);
            String subfix = room_name.substring(4,8);
            if( !isCourseNameExist(prefix) || !prefix.matches("[a-zA-Z]+") || !subfix.matches("[0-9]+" ) ){
                roomNameView.setError("Please Follow the Format: comp3111");
                cancel = true;
            }
        }

        if (cancel) {
            roomNameView.setText("");
            roomNameView.requestFocus();
        } else {
            // Start main activity
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra(ROOM_NAME, room_name);
            roomNameView.setText("");
            startActivity(intent);
        }
    }

    private boolean isEmailValid(String room_name) {
        // http://stackoverflow.com/questions/8248277
        // Make sure alphanumeric characters
        return !room_name.matches("^.*[^a-zA-Z0-9 ].*$");
    }

    public void addGridLayoutContent(String roomName, GridLayout gridLayout, int column){
        TextView viewToAdd = new TextView(this);
        viewToAdd.setWidth(280); //260 180
        viewToAdd.setHeight(280); //260 180
        viewToAdd.setClickable(true);
        viewToAdd.setPadding(0, 40, 0, 0);
        viewToAdd.setBackgroundColor(Color.WHITE);
        viewToAdd.setTextSize(14);
        viewToAdd.setTextColor(Color.parseColor("#607D8B"));
        viewToAdd.setGravity(Gravity.CENTER);
        viewToAdd.setText(roomName);

        // Setting margin of TextView
        GridLayout.LayoutParams param = new GridLayout.LayoutParams();
        if(column == 0)
            param.setMargins(0, 40, 0, 0);
        else
            param.setMargins(45, 40, 0, 0);
        viewToAdd.setLayoutParams(param);

        // Adding image to TextView
        Resources resources = this.getResources();

        String roomNamePrefix = "unknown";
        if( roomName.length() > 4 )
            roomNamePrefix = roomName.substring(0, 4);

        int id = resources.getIdentifier(roomNamePrefix, "drawable", this.getPackageName());
        viewToAdd.setCompoundDrawablesWithIntrinsicBounds(0, id, 0, 0); // set Drawable top

        viewToAdd.setOnClickListener(this);
        gridLayout.addView(viewToAdd);
    }

    private boolean isCourseNameExist(String input){
        for(String name : courseName){
            if(input.equals(name))
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        TextView tv = (TextView) view;
        String clickedRoom = tv.getText().toString();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(ROOM_NAME, clickedRoom);
        startActivity(intent);
    }

    public static void savePreference(String userName){
        JoinActivity.userName = userName;
        preference.edit().putString("userName", userName).commit();
    }

    public void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.setting_menu);
        popup.show();
    }
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changeName:
                showSetNameDialog();
                return true;
            case R.id.about:
                Toast.makeText(this, "Hello, we are love!sung", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return false;
        }
    }

    public void showSetNameDialog(){
        SetNameDialog dialog = new SetNameDialog();
        dialog.show(getFragmentManager(), "setNameDialog");
    }

    public void contactClick(View view){
        Toast.makeText(this, "Email: love!sung@gmail.com", Toast.LENGTH_SHORT).show();
    }

    public void featureClick(View view){
        getSupportFragmentManager().beginTransaction().addToBackStack("featureFragment").add(R.id.featureFragment, new FeatureFragment()).commit();
    }

}

