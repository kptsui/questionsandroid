package hk.ust.cse.hunkim.questionroom;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

public class ImageUploadActivity extends AppCompatActivity {

    private TextView roomName;
    private EditText inputMsg;
    private Bitmap bitmap;
    private ImageView imageView;
    private TextView alert;

    private int PICK_IMAGE_REQUEST = 1;

    private final String UPLOAD_URL ="http://52.24.244.209/android/php/upload.php";
    private final String KEY_IMAGE = "image";
    public static final int DATA_BACK_REQUEST = 7;

    private String header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        imageView = (ImageView) findViewById(R.id.imageView);
        inputMsg = (EditText) findViewById(R.id.inputMsg);
        alert = (TextView) findViewById(R.id.alert);

        header = getIntent().getStringExtra("header");
        roomName = (TextView) findViewById(R.id.roomName);
        roomName.setText(header);

        showFileChooser();
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            alert.setText("");
            Uri filePath = data.getData();

            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage(){
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Disimissing the progress dialog
                        loading.dismiss();

                        if(!response.equals("falied")){
                            // php: server response image url
                            Toast.makeText(ImageUploadActivity.this, "Message Sent", Toast.LENGTH_LONG).show();
                            String msg = inputMsg.getText().toString() + "\n" + response;

                            Intent intent = new Intent();
                            intent.putExtra("msg", msg);
                            setResult(RESULT_OK, intent);
                        }
                        else
                            Toast.makeText(ImageUploadActivity.this, "Connection Failed - Message Cannot Be Sent", Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();

                        //Showing toast
                        Toast.makeText(ImageUploadActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                Map<String,String> params = new Hashtable<String, String>();
                params.put(KEY_IMAGE, image);

                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public void backButtonClick(View v){
        finish();
    }

    public void buttonSendClick(View v){
        uploadImage();
    }
    public void imageButtonClick(View v){
        showFileChooser();
    }
}
